import Image from "next/image";
import Link from "next/link";
import React from "react";
import mooditorImg from "../public/assets/projects/mooditor.jpg";
import carImg from "../public/assets/projects/carcar.jpg";
import ProjectItem from "./ProjectItem";
import conferenceImg from "../public/assets/projects/ConferenceGo.png";

const Projects = () => {
  return (
    <div id="projects" className="w-full">
      <div className="max-w-[1240px] mx-auto px-2 py-16">
        <p className="text-xl tracking-widest uppercase text-[#0F5DC6]">
          Projects
        </p>
        <h2 className="py-4">What I&apos;ve Built</h2>
        <div className="grid md:grid-cols-2 gap-8">
          <ProjectItem
            title="mooditor"
            backgroundImg={mooditorImg}
            projectUrl="/mooditor"
            tech="FastAPI, React JS"
          />
          <ProjectItem
            title="Apex Automotive"
            backgroundImg={carImg}
            projectUrl="/apexauto"
            tech="Django, React JS"
          />
          <ProjectItem
            title="Conference GO!"
            backgroundImg={conferenceImg}
            projectUrl="/conferencego"
            tech="React JS"
          />
        </div>
      </div>
    </div>
  );
};

export default Projects;
