import React from "react";
import Image from "next/image";
import Link from "next/link";
import AboutImg from "../public/assets/about.jpg";

const About = () => {
  return (
    <div id="about" className="w-full md:h-screen p-2 flex items-center py-16">
      <div className="max-w-[1240px] m-auto md:grid grid-cols-3 gap-8">
        <div className="col-span-2">
          <p className="uppercase text-xl tracking-widest text-[#0F5DC6]">
            About
          </p>
          <h2 className="py-4">Who I Am</h2>
          <p className="py-2 text-gray-600">Hi everyone!</p>
          <p className="py-2 text-gray-600">
            I&apos;m Terence and I&apos;ve always had an interest in technology
            and how things worked, but it wasn&apos;t until I took a programming
            class in college that I realized my true passion for coding.
            I&apos;m skilled in several programming languages, including
            JavaScript, React, and Python, and have experience working with web
            development frameworks, databases, and APIs. I&apos;m particularly
            interested in working on projects that have a positive impact on
            people&apos;s lives. When I&apos;m not coding, you can find me
            hiking in the mountains, trying new foods, or going to the gym.
          </p>
          <Link href="/#projects">
            <p className="py-2 text-gray-600 underline cursor-pointer">
              Check out some of my latest projects.
            </p>
          </Link>
        </div>
        <div className="w-full h-auto m-auto shadow-xl shadow-gray-400 rounded-xl flex items-center justify-center p-4 hover:scale-105 ease-in duration-300">
          <Image src={AboutImg} className="rounded-xl" alt="/" />
        </div>
      </div>
    </div>
  );
};

export default About;
